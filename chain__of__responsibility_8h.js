var chain__of__responsibility_8h =
[
    [ "COR_FAILED", "chain__of__responsibility_8h.html#a3a50f86f40bfbffc1a8e26cc5a8f2874", null ],
    [ "COR_SUCCESS", "chain__of__responsibility_8h.html#a0bdf7aa097184001ff2e0e6dcf005664", null ],
    [ "chain_func", "chain__of__responsibility_8h.html#a733b83375fbe7a213908831d92901cc6", null ],
    [ "cor_result_e", "chain__of__responsibility_8h.html#a6d5dc8299fdfaa93cef94b6f41b42af7", [
      [ "CoR_GONEXT", "chain__of__responsibility_8h.html#a6d5dc8299fdfaa93cef94b6f41b42af7a5ffaea14fada81ba33c331934e351a4e", null ],
      [ "CoR_RETURN", "chain__of__responsibility_8h.html#a6d5dc8299fdfaa93cef94b6f41b42af7a6a09d1c5f5f398b1bab7bc1c40173a9b", null ]
    ] ],
    [ "cor_add_function", "chain__of__responsibility_8h.html#a7ab8871578e2e5e3c79f7ccbebe53b3e", null ],
    [ "cor_call", "chain__of__responsibility_8h.html#a6017534fc738e5fe592bc5b70a62b760", null ],
    [ "cor_clear", "chain__of__responsibility_8h.html#af7214f3ee33fd193ad8bc6ff19da970b", null ],
    [ "cor_remove_function", "chain__of__responsibility_8h.html#a992ce77a62711a598b767610c282ccee", null ]
];