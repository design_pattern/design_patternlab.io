var indexSectionsWithContent =
{
  0: "abcdefhilmnprst",
  1: "bcdflps",
  2: "bcdflps",
  3: "bcdflps",
  4: "abcdefhilmnprst",
  5: "bcdflps",
  6: "c",
  7: "c",
  8: "cdflps"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

